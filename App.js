import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import { createDrawerNavigator } from 'react-navigation-drawer';

// custom component
const Logo = () => <Text>HOME - TEST APP</Text>

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <Button
        title='Ir a detalle'
        // onPress={() => navigation.push('Detalle')}
        onPress={() => navigation.openDrawer()}
      />
    </View>
  );
}

HomeScreen.navigationOptions = {
  drawerIcon: ({tintColor}) => {
    return <Ionicons name="ios-information-circle" size={25} color={tintColor} />
  }, 
  title: 'Home',
  headerTitle: () => <Logo />,
  // 1ª forma de agregar botones
  //headerRight: () => <Button title='Sorpresa' color='#ee5' onPress={() => alert('FYI: Botón pulsado')} />,

  // headerStyle: {
  //   backgroundColor: 'lightsalmon',
  // },
  // headerTintColor: '#3d3d3d',
  // headerTitleStyle: {
  //   fontWeight: 'bold',
  // }
};

const DetalleScreen = ({ navigation }) => {
  const [cont, setCont] = useState(0);
  const increment = () => setCont(cont + 1);
  useEffect(() => {
    navigation.setParams({ increment });
  }, [cont]);

  const user = navigation.getParam('name', 'Valor por defecto');
  return (
    <View style={styles.container}>
      <Text>Soy la pantalla de detalle y este es el contador: {cont}</Text>
      <Button
        title='Navegar a modal'
        onPress={() => navigation.navigate('MyModal')}
      />
      <Button
        title='Cambiar título'
        onPress={() => navigation.setParams({ title: 'Usuario 1' })}
      />
    </View>
  );
}
// Con navigation options vamos a poder acceder a todas las configuraciones que
// se definieron en la configuración global.

DetalleScreen.navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam('title', 'Cargando...'),
    headerRight: () => {
      return <Button
        title='+ 1'
        color='black'
        onPress={navigation.getParam('increment')} />
    }
  };
}

const AppNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
  },
  Detalle: {
    screen: DetalleScreen,
  }
}, {
  /** Configuración inicial para toda la aplicación */
  initialRouteName: 'Home',
});

const RootStack = createStackNavigator({
  Main: AppNavigator,
  MyModal: () => <Text>Hola qué tal!!!</Text>
}, {
  mode: 'modal',
  headerMode: 'none'
});

export default createAppContainer(RootStack);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
